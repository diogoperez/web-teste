import React, { useState, useEffect } from 'react';

function App() {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch('https://api-teste-ij4g.onrender.com/usuarios'); // Substitua 'localhost:4000' pela sua URL de API
                const data = await response.json();
                setUsers(data);
            } catch (error) {
                console.error('Erro ao obter usuários:', error);
            }
        }
        fetchData();
    }, []); // Este efeito será executado apenas uma vez, quando o componente for montado

    return (
        <div>
            <h1>Lista de Usuário</h1>
            <ul>
                {users.map(user => (
                    <li key={user.id}>
                        <strong>{user.nome}</strong>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default App;
